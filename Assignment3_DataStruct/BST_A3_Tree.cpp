#include<iostream>
#include<fstream>
#include<string>

#define MaxWordSize 20
using namespace std; 
// define a structure for the data
typedef struct {
	char  word[MaxWordSize + 1];
} NodeData;

// define the structure of treenode
typedef struct treenode {
	NodeData data;
	struct treenode *left, *right;
}TreeNode, *TreeNodePtr;

// defin the binary tree

typedef struct {
	TreeNodePtr root;
}BinaryTree;

// write a function to build the tree
TreeNodePtr buildTree(std::fstream *in) {
	char str[MaxWordSize + 1];
	// read data from the file 
	// and stop at the first white space
	//string str;
	*in>>str;

	if (strcmp(str, "@") == 0) return NULL;	
	// create a note if the data is not null 
	TreeNodePtr p = new TreeNode;
	strcpy_s(p->data.word, str);
	// call build tree to build the left side of the tree
	p->left = buildTree(in);
	// the in file pointer moves after each reading process
	p->right = buildTree(in);
	return p;

}

void visit(TreeNodePtr node) {
	cout<< node->data.word;
}

void preOrder(TreeNodePtr node) {
	if (node != NULL) {
		visit(node);
		preOrder(node->left);
		preOrder(node->right);
	}
}

int main() {
	// declare the binary tree variable 
	BinaryTree bt;
	fstream in;
	
	in.open("btree.txt");
	if (in.is_open()) {
		//build array


		// build the tree
		bt.root = buildTree(&in);
		cout << "\n" << " the pre-order trevesal is:";
		preOrder(bt.root); // print the tree from the memory
	}
	else cout << " cannot open the file";
	system("pause"); 

}