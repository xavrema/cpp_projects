#include<iostream>
#include<fstream>
#include<string>
#include<sstream>
#include<vector>
using namespace std;
void getFromFile(string[]);

typedef struct TreeNode {
	string last_name;
	string first_name;
	string company;
	string phone;
	string address;
	string job;
	string title;
	double annual_income;
	struct TreeNode *left, *right;
}TreeNode, *TreeNodePtr;


typedef struct {
	TreeNodePtr root;
}BinaryTree;


TreeNodePtr buildTree(string arr[]) {
	void addNode(TreeNodePtr, TreeNodePtr);
	TreeNodePtr split_obj(string);
	// read data from the file 
	// and stop at the first white space
	//string str;

	// create a note if the data is not null 
	TreeNodePtr p = split_obj(arr[0]);

	for (int i = 1; i < 200; i++) {
		TreeNodePtr newNode = split_obj(arr[i]);
		addNode(p, newNode);
	}

	return p;

}

void addNode(TreeNodePtr cNode, TreeNodePtr cNum) {
	if (cNum->last_name[0] < cNode->last_name[0]) {
		if (cNode->left != NULL) {
			addNode(cNode->left, cNum);
		}
		else {
			cNode->left = cNum;
		}
	}
	else if (cNum->last_name[0] > cNode->last_name[0]) {
		if (cNode->right != NULL) {
			addNode(cNode->right, cNum);
		}
		else {
			cNode->right = cNum;
		}
	}
	else {
		int count = 0;
		if (cNode->last_name != cNum->last_name) {
			while (cNode->last_name[count] == cNum->last_name[count]) {

				count += 1;
			}
		}
		if (count == cNode->last_name.length()) {
			if (cNode->right != NULL) {
				addNode(cNode->right, cNum);
			}
			else {
				cNode->right = cNum;
			}
		}
		else {
			if (cNum->last_name[count] < cNode->last_name[count]) {
				if (cNode->left != NULL) {
					addNode(cNode->left, cNum);
				}
				else {
					cNode->left = cNum;
				}
			}
			else if (cNum->last_name[count] > cNode->last_name[count]) {
				if (cNode->right != NULL) {
					addNode(cNode->right, cNum);
				}
				else {
					cNode->right = cNum;
				}
			}
		}
	}
}

void getFromFile(string userData[]) {
	ifstream in;
	in.open("users_data.txt");
	int count = 0;
	string user;
	if (in.is_open()) {
		while (getline(in, user)) {
			userData[count] = user;
			count++;
		}
	}

}


TreeNodePtr split_obj(string userRaw) {
	vector<string> userInf;
	TreeNodePtr user = new TreeNode;
	stringstream ss(userRaw);


	while (ss.good()) {
		string temp;
		getline(ss, temp, ',');
		userInf.push_back(temp);
	}
	user->first_name = userInf.at(0);
	user->last_name = userInf.at(1);
	user->company = userInf.at(2);
	user->phone = userInf.at(3);
	user->job = userInf.at(4);
	user->address = userInf.at(5);
	user->title = userInf.at(6);
	user->annual_income = stod(userInf.at(7));
	user->left = NULL;
	user->right = NULL;

	return user;
}

string userValidate() {
	string temp;
	cout << "Please Enter a Last Name to Search for :";
	cin >> temp;
	return temp;
}

TreeNodePtr findNode(TreeNodePtr p, string userInput) {
	if (userInput != p->last_name) {
		int count = 0;
		while (userInput[count] == p->last_name[count]) {
			count += 1;
		}
		if (userInput[count] > p->last_name[count]) {
			if (p->right != NULL) {
				p = findNode(p->right, userInput);
			}
		}
		else if (userInput[count] < p->last_name[count]) {
			if (p->left != NULL) {
				p = findNode(p->left, userInput);
			}
		}
	}
	return p;


}

TreeNodePtr searchTree(TreeNodePtr root) {
	string userIn = userValidate();
	TreeNodePtr record = findNode(root, userIn);

	if (userIn != record->last_name) {
		cout << "Record Not Found" << endl;
	}
	else {
		cout << "Record Found" << endl;
		cout << record->last_name << endl;
	}
	return record;


}

void createNode(TreeNodePtr root) {
	TreeNodePtr record = new TreeNode;
	string info;
	cout << "Enter The Following Information" << endl;
	cout << "[1] First Name: ";
	cin >> record->first_name;
	cout << "[2] Last Name: ";
	cin >> record->last_name;
	cout << "[3] Company: ";
	cin >> record->company;
	cout << "[4] Phone: ";
	cin >> record->phone;
	cout << "[5] Job: ";
	cin >> record->job;
	cout << "[6] Address: ";
	cin >> record->address;
	cout << "[7] Title: ";
	cin >> record->title;
	cout << "[8] Annual Income: ";
	try {
		cin >> info;
		record->annual_income = stod(info);
	}
	catch (exception e) {
		cout << "Invalid Income" << endl;
		cout << "Im making this hard on you so you dont enter invalid input again MU-HAHAHA" << endl;
		createNode(root);
	}
	addNode(root, record);
	cout << "Record Added" << endl;

}

void deleteRecord(TreeNodePtr root, TreeNodePtr record) {
	TreeNodePtr temp;
	if (record->left == NULL && record->right == NULL) {
		cout << "Record Deleted" << endl;
		record = NULL;
		return;

	}
	else if (record->left == NULL)
	{	
		record = record->right;
		cout << "Record Deleted" << endl;
		return;
	}
	else if (record->right == NULL)
	{
		record = record->left;
		cout << "Record Deleted" << endl;
		return;
	}
	temp = record;
	record = NULL;
	addNode(root, temp->left);
	addNode(root, temp->right);

	cout << "Record Deleted" << endl;
}

void sortTree(TreeNodePtr root) {
	if (root == NULL) {
		return;
	}
	sortTree(root->left);
	cout << root->last_name << endl;
	sortTree(root->right);

}


void disMenu(TreeNodePtr root) {
	cout << "Select an Option by Index" << endl;
	cout << "[1] Add" << endl;
	cout << "[2] Retrieve" << endl;
	cout << "[3] Delete" << endl;
	cout << "[4] Sort" << endl;
	cout << "[5] Save" << endl;
	cout << "[6] Exit" << endl;
	int userIn;
	try {
		cin >> userIn;
		if (userIn > 6 || userIn<1) {
			throw invalid_argument("");
		}
		switch (userIn) {
		case 1: cout << "You Selected Add" << endl;
			createNode(root);
			break;
		case 2: cout << "You Selected Retrieve" << endl;
			searchTree(root);
			break;
		case 3: cout << "You Selected Delete" << endl;
			deleteRecord(root, searchTree(root));
			break;
		case 4: cout << "You Selected Sort" << endl;
			sortTree(root);
			break;
		case 5: cout << "You Selected Save" << endl; break;

		case 6: exit(0);
			break;
		default: cout << "default" << endl;

		}
	}
	catch (exception e) {
		cout << "Please Enter a Valid Choice" << endl;
		disMenu(root);
	}
	disMenu(root);


}

int main() {
	string userData[200];
	string temp;
	getFromFile(userData);
	BinaryTree bt;
	bt.root = buildTree(userData);
	disMenu(bt.root);

	system("pause");


}
